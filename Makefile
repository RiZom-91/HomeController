# Project properties
PROJECTNAME   = HomeController

# Project directory structure
INCLUDEDIR    = include
SRCDIR        = src
OBJDIR        = obj
BINDIR        = bin
BINNAME       = $(PROJECTNAME)

# Project files
TARGET        = $(BINDIR)/$(BINNAME)
SOURCES       = $(wildcard $(SRCDIR)/*.cpp)
OBJECTS       = $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)

# Build Options
ifeq ($(CXX),)
  CXX         = g++
endif
INCLUDE       = -I$(INCLUDEDIR)
CXXFLAGS     := -Wall
LDFLAGS      := -lpthread -lwiringPi

# Installation options
ifeq ($(INSTALLDIR),)
  INSTALLDIR  = /usr/local/bin
endif

# Commands
MKDIR         = mkdir -p
RM            = rm -Rf
CP            = cp

# make
all: $(TARGET)

# Build the executable
$(TARGET) : init $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(INCLUDE) $(LDFLAGS) $(OBJECTS) -o $@

# Create output directories
init:
	$(MKDIR) $(OBJDIR)
	$(MKDIR) $(BINDIR)

# Compile all objects
$(OBJECTS) : $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) $(LDFLAGS) -c $< -o $@

# make clean
clean:
	$(RM) $(BINDIR)
	$(RM) $(OBJDIR)

# make install
install:
	$(CP) $(TARGET) $(DESTDIR)

# make uninstall
uninstall:
	$(RM) $(DESTDIR)/$(BINAME)

.PHONY: clean, init, install, uninstall
