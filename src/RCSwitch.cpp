#include "RCSwitch.h"
#include "RadioSender.h"
#include "Util.h"

RCSwitch::RCSwitch(const std::string &name, unsigned int code):
    m_name(name),
    m_code(code),
    m_state(UNKNOWN)
{
    Util::Log(Util::INFO, LOG_SENDER, "'" + m_name + "' has been created");
}

RCSwitch::~RCSwitch()
{
    Util::Log(Util::INFO, LOG_SENDER, "'" + m_name + "' has been deleted");
}

void RCSwitch::SwitchOn(RadioSender *sender)
{
    if (sender == nullptr)
    {
        Util::Log(Util::ERROR, LOG_SENDER, "'" + m_name + "' could not be switched off because the sender was null");
        return;
    }

    sender->send(m_code, 24);

    if (m_state != ON)
    {
        m_state = ON;
        Util::Log(Util::INFO, LOG_SENDER, "'" + m_name + "' has been switched on");
    }
}

void RCSwitch::SwitchOff(RadioSender *sender)
{
    if (sender == nullptr)
    {
        Util::Log(Util::ERROR, LOG_SENDER, "'" + m_name + "' could not be switched off because the sender was null");
        return;
    }

    sender->send(m_code-4, 24);

    if (m_state != OFF)
    {
        m_state = OFF;
        Util::Log(Util::INFO, LOG_SENDER, "'" + m_name + "' has been switched off");
    }
}
