#include <unistd.h>
#include "DHT22Reader.h"
#include "RadioSender.h"
#include "RCSwitch.h"
#include "Room.h"
#include "Util.h"
#include "wiringPi.h"

int main()
{

    // Initialize the wiringPi library
    if (wiringPiSetup() != 0)
    {
        Util::Log(Util::ERROR, "main", "Failed to initialize wiringPi library");
        return -1;
    }

    // Create a room instance for the living room
    Room* livingRoom = new Room("Living Room");
    livingRoom->SetSender(new RadioSender(2));
    livingRoom->SetSensor(new DHT22Reader(0));
    livingRoom->AddRadioHeater(new RCSwitch("heater1", 9366952));
    livingRoom->AddRadioHeater(new RCSwitch("heater2", 9366952));
    livingRoom->SetTargetTemperature(18);

    // Infinite control loop
    while(true)
    {
        livingRoom->Control();

        // Sleep for 30s
        sleep(30);
    }

    delete livingRoom;

    return 0;
}
