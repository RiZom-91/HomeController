#include "DHT22Reader.h"
#include <unistd.h>
#include "Util.h"
#include "wiringPi.h"

DHT22Reader::DHT22Reader(int pinNum):
    m_PinNum(pinNum),
    m_Temperature(-255),
    m_Humidity(-255),
    m_lastReading(0),
    m_lastResult(DHT_ERROR_TIMEOUT)
{
}

DHT22Reader::~DHT22Reader()
{
}

int DHT22Reader::Read()
{
    // The sensor needs a minimum delay of 2s between two readings
    std::time_t now = std::time(nullptr);
    if ((now - m_lastReading) <= DHT_READ_INTERVAL)
    {
        return m_lastResult;
    }

    // Update last reading time
    m_lastReading = now;

    uint8_t laststate = HIGH;
    int counter = 0;
    uint8_t j = 0, i;
    int data[5] = {};
    int pulse_length[DHT_MAX_PULSES] = {};

    /****************
     * Start signal *
     ****************/

    // Set pin mode to output
    pinMode(m_PinNum, OUTPUT);

    // Set pin HIGH for 250ms to reset sensor
    digitalWrite(m_PinNum, HIGH);
    delay(250);

    // Set pin low for 10 milliseconds (datasheet: at least 1ms)
    digitalWrite(m_PinNum, LOW);
    delay(10);

    // Set highest priority while reading
    piHiPri(99);


    // Then set pin high (datasheet: 20~40us)
    digitalWrite(m_PinNum, HIGH);

    /*******************
     * Sensor response *
     *******************/

    // Prepare to read the pin
    pinMode(m_PinNum, INPUT);

    // Detect changes and read data
    for (i=0; i < DHT_MAX_PULSES; i++)
    {
        counter = 0;
        while (digitalRead(m_PinNum) == laststate)
        {
            counter++;
            // Handle timeout
            if (counter >= DATA_TIMEOUT)
            {
                m_lastResult = DHT_ERROR_TIMEOUT;
                return m_lastResult;
            }
        }

        // Store pulse length
        pulse_length[i]=counter;

       // Store last input state
        laststate = digitalRead(m_PinNum);
    }

    // Reset priority now that reading is done
    piHiPri(0);

    int threshold = 0;

    // Calculate threshold between bit = 0 and bit = 1
    for (i = 3 ; i < DHT_MAX_PULSES ; i += 2)
    {
        threshold += pulse_length[i];
    }
    threshold /= 40; // 40 bits

    // Construct data
    for (i = 4 ; i < DHT_MAX_PULSES ; i += 2)
    {
        // shove each bit into the storage bytes
        data[j/8] <<= 1;

        // Set new bit as 1 depending on the length of the signal
        if (pulse_length[i] > threshold)
        {
            data[j/8] |= 1;
        }

        // Next bit
        j++;
    }

    /*************
     * Read data *
     *************/

    // verify checksum in the last byte
    if (data[4] != ((data[0] + data[1] + data[2] + data[3]) & 0xFF))
    {
        m_lastResult = DHT_ERROR_CHECKSUM;
        return m_lastResult;
    }

    // Data calculation
    float t, h;
    h = ((float)data[0] * 256 + (float)data[1]) / 10.0f;
    t = ((float)(data[2] & 0x7F)* 256 + (float)data[3]) / 10.0f;
    if ((data[2] & 0x80) != 0)  t *= -1;

    // Finally update humidity and temperature values
    m_Humidity = h;
    m_Temperature = t;

    // SUCCESS!
    m_lastResult = DHT_SUCCESS;
    return m_lastResult;
}
