#include "Util.h"
#include <iostream>

void Util::Log(LogLevel level, std::string sender, std::string message)
{
    // TODO log to file
    // TODO DO NOT TRUST WHAT I GOT

    std::string lvl;
    std::string result;

    switch(level)
    {
        case DEBUG:
            lvl = "[DEBUG] ";
            break;
        case INFO:
            lvl = "[INFO ] ";
            break;
        case WARNING:
            lvl = "[WARN ] ";
            break;
        case ERROR:
            lvl = "[ERROR] ";
            break;
        default:
            Log(ERROR, "Log", "Tried to log a message with an unknown level");
            return;
    }

    result += lvl;

    if (sender != "")
    {
        sender = "[" + sender + "] ";
        result += sender;
    }

    result += message;

    std::cout << result << std::endl;
}
