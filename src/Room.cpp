#include "Room.h"

#include <unistd.h>
#include "DHT22Reader.h"
#include "RadioSender.h"
#include "RCSwitch.h"
#include "Util.h"

Room::Room(std::string name):
    m_Name(name),
    m_Sensor(nullptr),
    m_sender(nullptr),
    m_TargetTemperature(0),
    m_Heating(false)
{
    Util::Log(Util::INFO, LOG_SENDER, m_Name + " has been created");
}

Room::~Room()
{
    Util::Log(Util::INFO, LOG_SENDER, m_Name + " is being deleted");

    // Delete the sensor instance
    DeleteSensor();

    // Remove all heaters
    if (m_RadioHeaters.size() > 0)
    {
        for(unsigned int i = 0 ; i < m_RadioHeaters.size() ; i++)
        {
            m_RadioHeaters.at(i)->SwitchOff(m_sender);
            delete m_RadioHeaters.at(i);
            m_RadioHeaters.at(i) = nullptr;
        }
        m_RadioHeaters.clear();
    }

    // Delete the radio sender instance
    DeleteSender();

    Util::Log(Util::INFO, LOG_SENDER, m_Name + " has been deleted");
}

void Room::SetSensor(DHT22Reader* sensor)
{
    // Delete any previous sensor
    DeleteSensor();

    if (sensor == nullptr)
    {
        Util::Log(Util::ERROR, LOG_SENDER, "Tried to add a sensor that is NULL in " + m_Name);
        return;
    }

    // Set current sensor
    m_Sensor = sensor;

    Util::Log(Util::INFO, LOG_SENDER, "Sensor has been set for " + m_Name);
}

void Room::SetSender(RadioSender* sender)
{
    // Delete any previous sender
    DeleteSender();

    if (sender == nullptr)
    {
        Util::Log(Util::ERROR, LOG_SENDER, "Tried to add a radio sender that is NULL in " + m_Name);
        return;
    }

    // Set current sender
    m_sender = sender;

    Util::Log(Util::INFO, LOG_SENDER, "Radio sender has been set for " + m_Name);
}

void Room::AddRadioHeater(RCSwitch* heater)
{
    if (heater == nullptr)
    {
        Util::Log(Util::ERROR, LOG_SENDER, "Tried to add a heater that is NULL in " + m_Name);
        return;
    }

    if (FindRadioHeater(heater->GetName()) != nullptr)
    {
        Util::Log(Util::ERROR, LOG_SENDER, m_Name + " already has a heater called " + heater->GetName());
        return;
    }

    m_RadioHeaters.push_back(heater);
    Util::Log(Util::INFO, LOG_SENDER, "Heater '" + heater->GetName() + "' has been added in " + m_Name);
}

RCSwitch* Room::FindRadioHeater(std::string name)
{
    RCSwitch* result = nullptr;

    if (m_RadioHeaters.size() > 0)
    {
        for (unsigned int i = 0 ; i < m_RadioHeaters.size() ; i++)
        {
            if (m_RadioHeaters.at(i)->GetName() == name)
            {
                result = m_RadioHeaters.at(i);
                break;
            }
        }
    }

    if (result == nullptr) {
        Util::Log(Util::WARNING, LOG_SENDER, m_Name + ": could not find any heater called '" + name + "'");
    }

    return result;
}

void Room::SetTargetTemperature(double temp)
{
    if (temp == m_TargetTemperature) return;

    if (temp > 22 || temp < 10)
    {
        Util::Log(Util::ERROR, LOG_SENDER, m_Name + ": Out of range target temperature " + std::to_string(temp));
        return;
    }

    // Update target temperature
    m_TargetTemperature = temp;
    Util::Log(Util::INFO, LOG_SENDER, "Target temperature is set to " + std::to_string(temp) + " in " + m_Name);

    // Turn ON/OFF heaters if necessary
    Control();
}

float Room::GetTemperature() const
{
    if (m_Sensor != nullptr)
        return m_Sensor->GetTemperature();
    else
        return -300;
}

float Room::GetHumidity() const
{
    if (m_Sensor != nullptr)
        return m_Sensor->GetHumidity();
    else
        return -300;
}

void Room::DeleteSensor()
{
    if (m_Sensor != nullptr)
    {
        // Delete sensor
        delete m_Sensor;
        m_Sensor = nullptr;
        Util::Log(Util::INFO, LOG_SENDER, "Sensor has been deleted from " + m_Name);
    }
}

void Room::DeleteSender()
{
    if (m_sender != nullptr)
    {
        // Delete sender
        delete m_sender;
        m_sender = nullptr;
        Util::Log(Util::INFO, LOG_SENDER, "Radio sender has been deleted from " + m_Name);
    }
}

bool Room::ReadSensor()
{
    if (m_Sensor == nullptr) {
        Util::Log(Util::ERROR, LOG_SENDER, "No sensor has been defined for " + m_Name);
        return false;
    }

    bool success = false;

    for (unsigned int i = 0 ; i < 2 ; i++)
    {
        // Try to read data from sensor
        if (m_Sensor->Read() == DHT_SUCCESS)
        {
            // Success
            success = true;
            break;
        }

        // We need to wait before reading again
        sleep(DHT_READ_INTERVAL);
    }

    if (!success) {
        Util::Log(Util::ERROR, LOG_SENDER, "Failed to read data from sensor of " + m_Name);
    }
    else {
        Util::Log(Util::INFO, LOG_SENDER, m_Name + ": " + std::to_string(GetTemperature()) + "°C, " + std::to_string(GetHumidity()) + "%");
    }

    return success;
}

void Room::Control()
{
    // Return if we failed to read data from sensor
    if (ReadSensor() == false) return;

    if (GetTargetTemperature() > GetTemperature())
    {
        if (m_RadioHeaters.size() > 0)
        {
            // Only log something when heating state has changed
            if (!m_Heating)
            {
                m_Heating = true;
                Util::Log(Util::INFO, LOG_SENDER, "Switching heaters ON: " + m_Name);
            }

            for (unsigned int i = 0 ; i < m_RadioHeaters.size() ; i++) {
                m_RadioHeaters.at(i)->SwitchOn(m_sender);
            }
        }
        else
        {
            Util::Log(Util::WARNING, LOG_SENDER, "No heater to turn ON: " + m_Name);
        }
    }
    else
    {
        if (m_RadioHeaters.size() > 0)
        {
            // Only do something when heating state has changed
            if (m_Heating)
            {
                m_Heating = false;
                Util::Log(Util::INFO, LOG_SENDER, "Switching heaters OFF: " + m_Name);
            }

            for (unsigned int i = 0 ; i < m_RadioHeaters.size() ; i++) {
                m_RadioHeaters.at(i)->SwitchOff(m_sender);
            }
        }
        else
        {
            Util::Log(Util::WARNING, LOG_SENDER, "No heater to turn OFF: " + m_Name);
        }
    }
}
