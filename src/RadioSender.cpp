#include "RadioSender.h"
#include "Util.h"
#include "wiringPi.h"

RadioSender::RadioSender(unsigned int pinNum, unsigned int repeatCount):
    m_pin(pinNum),
    m_repeatCount(repeatCount)
{
    enableTransmit();
    Util::Log(Util::INFO, LOG_SENDER, "Instance has been created for pin " + std::to_string(m_pin));
}

RadioSender::~RadioSender()
{
    disableTransmit();
    Util::Log(Util::INFO, LOG_SENDER, "Instance has been deleted for pin " + std::to_string(m_pin));
}

void RadioSender::enableTransmit()
{
    pinMode(m_pin, OUTPUT);
    Util::Log(Util::INFO, LOG_SENDER, "Transmit has been enabled on pin " + std::to_string(m_pin));
}

void RadioSender::disableTransmit()
{
    // Set output LOW and disable OUTPUT mode
    digitalWrite(m_pin, LOW);
    pinMode(m_pin, INPUT);
    Util::Log(Util::INFO, LOG_SENDER, "Transmit has been disabled on pin " + std::to_string(m_pin));
}

void RadioSender::send(long code, unsigned int length)
{
    // Repeat transmission
    for (unsigned j = 0 ; j < 3 ; j++)
    {
        // Send each bit as pulses
        for (int i = length-1; i >= 0; i--)
        {
            if (code & (1L << i))
                transmit(ONE);
            else
                transmit(ZERO);
        }

        // End transmission with SYNC pulses
        transmit(SYNC);

        // Make sure we set the output LOW after sending
        digitalWrite(m_pin, LOW);
    }
}

void RadioSender::transmit(HighLow pulses)
{
  digitalWrite(m_pin, HIGH);
  delayMicroseconds(PULSE_LENGTH * pulses.high);
  digitalWrite(m_pin, LOW);
  delayMicroseconds(PULSE_LENGTH * pulses.low);
}

