#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <thread>
#include <vector>

class DHT22Reader;
class RadioSender;
class RCSwitch;

class Room
{
    public:

        /**
         * @brief Constructor
         * @param name, the name of the room
         */
        Room(std::string name);

        /**
         * @brief Destructor
         */
        virtual ~Room();

        /**
         * @brief Set the sensor used by the room
         * @param sensor, the sensor instance
         */
        void SetSensor(DHT22Reader* sensor);

        /**
         * @brief Set the radio sender used by the room
         * @param sensor, the sender instance
         */
        void SetSender(RadioSender* sender);

        /**
         * @brief Add a heater instance to the room
         * @param heater, the heater instance to add
         */
        void AddRadioHeater(RCSwitch* heater);

        /**
         * @brief Set the target temperature of the room
         * @param temp, the target temperature to set
         */
        void SetTargetTemperature(double temp);

        /**
         * @brief Get the current temperature of the room
         * @return The temperature of the room
         */
        float GetTemperature() const;

        /**
         * @brief Get the current humidity of the room
         * @return The humidity of the room
         */
        float GetHumidity() const;

        /**
         * @brief Get a flag indicating whether heaters are ON or OFF
         * @return true if heaters are ON, false otherwise
         */
        bool IsHeating() const { return m_Heating; }

        /**
         * @brief Acces the name of the room
         * @return The name of the room
         */
        std::string GetName() const { return m_Name; }

        /**
         * @brief Get the target temperature of the room
         * @return The target temperature of the room
         */
        double GetTargetTemperature() const { return m_TargetTemperature; }

        /**
         * @brief Read data srom sensor (automatically called from Control() method)
         * @return true for success, false for errors
         */
        bool ReadSensor();

        /**
         * @brief Turn ON/OFF heaters depending on the current and the target temperatures
         */
        void Control();

    private:

        /**
         * @brief Delete the sensor used by the room
         */
        void DeleteSensor();

        /**
         * @brief Delete the radio sender used by the room
         */
        void DeleteSender();

        /**
         * @brief Find a heater in the room
         * @param name, the name of the heater to search
         * @return nullptr if the heater doesn't exist, the heater pointer otherwise
         */
        RCSwitch* FindRadioHeater(std::string name);

        std::string m_Name;
        DHT22Reader* m_Sensor;
        RadioSender* m_sender;
        std::vector<RCSwitch*> m_RadioHeaters;
        double m_TargetTemperature;
        bool m_Heating;
        const std::string LOG_SENDER = "Room";
};

#endif // ROOM_H
