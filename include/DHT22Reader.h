#ifndef DHT22READER_H
#define DHT22READER_H

// Define errors and return values.
#define DHT_ERROR_TIMEOUT -1
#define DHT_ERROR_CHECKSUM -2
#define DHT_SUCCESS 0

// Define the maximum pulses sensor gives during a reading operation
#define DHT_MAX_PULSES 84

// Define the timeout value (arbitrary value from testing for RPi 1)
#define DATA_TIMEOUT 1000

// Minimum time in second between two sensor readings
#define DHT_READ_INTERVAL 2

#include <ctime>
#include <string>
#include <vector>

class DHT22Reader
{
    public:

        /**
         * @brief Constructor
         * @param m_PinNum, the number of the pin the sensor is connected to ((wiringPi numbers))
         */
        explicit DHT22Reader(int pinNum);

        /**
         * @brief Destructor
         */
        virtual ~DHT22Reader();

        /**
         * @brief Access the last temperature reading of the sensor
         * @return The value of the last temperature reading
         */
        float GetTemperature() const { return m_Temperature; }

        /**
         * @brief Access the last humidity reading of the sensor
         * @return The value of the last humidity reading
         */
        float GetHumidity() const { return m_Humidity; }

        /**
         * @brief Read data from the sensor. WARNING!!! wiringPi needs to be setup before
         * calling this method.
         * @return 0 for success, less than 0 for errors
         */
        int Read();

    private:

        int m_PinNum;
        float m_Temperature;
        float m_Humidity;
        std::time_t m_lastReading;
        int m_lastResult;

        const std::string LOG_SENDER = "DHT22Reader";

};

#endif // DHT22READER_H
