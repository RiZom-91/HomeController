#ifndef RADIOSENDER_H
#define RADIOSENDER_H

#include <string>

class RadioSender
{

    public:

        /**
         * @brief Constructor
         * @param pinNum, the pin used to send radio signals
         * @param repeatCount, how many times signals should be repeated
         */
        RadioSender(unsigned int pinNum, unsigned int repeatCount = 10);

        /**
         * @brief Constructor
         */
        ~RadioSender();

        /**
         * @brief Send a radio signal as binary literal expression (e.g. '0b010100101')
         * @param code, the code integer to send (e.g '0b010100101' or '325416')
         * @param length, the length in bits of the code to send
         */
        void send(long code, unsigned int length);

    private:

        /**
         * @brief Define a High/Low pulses count
         */
        struct HighLow {
            uint8_t high;
            uint8_t low;
        };

        /**
         * @brief Enable transmissions of signals
         */
        void enableTransmit();

        /**
         * @brief Disable transmissions of signals
         */
        void disableTransmit();

        /**
         * @brief Transmit a HighLow pulses couple
         * @param pulses, the HighLow pulses couple
         */
        void transmit(HighLow pulses);

        unsigned int m_pin;
        unsigned int m_repeatCount;

        // Radio protocol for Emylo radio switches
        // Determined with 433Utils/RPi_utils/RFSniffer
        // and rc-switch library
        const int         PULSE_LENGTH   = 290;
        const HighLow     SYNC           = {1,31};
        const HighLow     ONE            = {3,1};
        const HighLow     ZERO           = {1,3};

        // Only use for logging
        const std::string LOG_SENDER     = "RadioSender";
};

#endif
