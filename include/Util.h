#ifndef UTIL_H
#define UTIL_H

#include <string>

class Util
{
    public:

        /**
         * @brief The list of possible levels for log messages
         */
        enum LogLevel
        {
            DEBUG,
            INFO,
            WARNING,
            ERROR
        };

        /**
         * @brief Log a message
         * @param level, the level of the message
         * @param sender, the sender of the message
         * @param message, the message to log
         */
        static void Log(LogLevel level, std::string sender, std::string message);

    protected:

    private:
};

#endif // UTIL_H
