#ifndef RCSWITCH_H
#define RCSWITCH_H

#include <string>

class RadioSender;

class RCSwitch
{
    public:

        enum State
        {
            UNKNOWN,
            ON,
            OFF
        };

        /**
         * @brief Constructor
         */
        RCSwitch(const std::string &name, unsigned int code);

        /**
         * @brief Destructor
         */
        virtual ~RCSwitch();

        /**
         * @brief Send the "switch on" message to the remote device
         * @param sender, the radio sender instance used to send the message
         */
        void SwitchOn(RadioSender* sender);

        /**
         * @brief Send the "switch off" message to the remote device
         * @param sender, the radio sender instance used to send the message
         */
        void SwitchOff(RadioSender* sender);

        /**
         * @brief Acces the name of the radio switch
         * @return The name of the radio switch
         */
        std::string GetName() const { return m_name; }

        State getState() const { return m_state; }

    private:

        std::string m_name;
        unsigned int m_code;
        State m_state;

        std::string LOG_SENDER = "RCSwitch";
};

#endif // RCSWITCH_H
